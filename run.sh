#!/bin/bash
#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier: GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

test -t 1 && USE_TTY="-ti"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

run() {
    docker run --rm ${USE_TTY}  \
        --network host \
        --env-file $SCRIPT_DIR/.env \
        gitlab.adullact.net:4567/datatourisme/quality-analyzer:master \
        $1
}

run analyze
run statistics
run purge
