/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import com.github.pemistahl.lingua.api.IsoCode639_1;
import com.github.pemistahl.lingua.api.Language;
import com.github.pemistahl.lingua.api.LanguageDetector;
import com.github.pemistahl.lingua.api.LanguageDetectorBuilder;
import fr.datatourisme.quality.entity.Anomaly;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Détection de code HTML
 * Horaires ou tarifs présents dans les descriptifs (anglais et français)
 * Vérification de la cohérence de la langue dans les propriétés localisées
 */
@Component
public class TextAnalyzer extends AbstractAsyncAnalyzer {

    static private final String[] paths = new String[] {
        "hasDescription/shortDescription/*",
        "hasDescription/description/*",
        "COVID19SpecialMeasures/*",
        "//openingDetails/*",
        "offers//additionalInformation/*"
    };

    private final Pattern htmlPattern = Pattern.compile("<(\"[^\"]*\"|'[^']*'|[^'\">])*>");

    static private final Language[] LANGUAGES = new Language[] {
        Language.FRENCH,
        Language.ENGLISH,
        Language.DUTCH,
        Language.GERMAN,
        Language.SPANISH,
        Language.ITALIAN
    };
    private final Pattern languagePattern = Pattern.compile("@([^\\.]+)");
    private final double minimumLanguageConfidence = 0.85d;
    final LanguageDetector detector;

    private final Pattern schedulePattern = Pattern.compile("\\b\\d+h(\\d+)*\\b");
    private final Pattern pricesPattern = Pattern.compile("\\beuros|€\\b");

    public TextAnalyzer() {
        detector = LanguageDetectorBuilder.fromLanguages(LANGUAGES).build();
    }

    @Override
    public List<Anomaly> analyze(AnalyzeItem item) {
        List<Anomaly> anomalies = new ArrayList<>();

        Map<String, String> map = item.getValues(paths);
        map.forEach((path, text) -> {

            // Verify HTML
            Matcher matcher = htmlPattern.matcher(text);
            if (matcher.find()) {
                anomalies.add(new Anomaly()
                    .setPath(path)
                    .setValue(text)
                    .setMessage("Contenu HTML")
                    .setDescription("Du code HTML a été détecté dans le texte")
                );
            }

            // Verify prices & schedule
            if (!path.contains("openingDetails")) {
                /* matcher = schedulePattern.matcher(text);
                if (matcher.find()) {
                    anomalies.add(item.createAnomaly()
                        .setPath(path)
                        .setValue(text)
                        .setMessage("Information d'horaires d'ouverture")
                        .setDescription("Des informations d'horaire d'ouverture ont été trouvées dans un descriptif.")
                    );
                }*/
                matcher = pricesPattern.matcher(text);
                if (matcher.find()) {
                    anomalies.add(new Anomaly()
                        .setPath(path)
                        .setValue(text)
                        .setMessage("Information de tarification")
                        .setDescription("Des informations de tarification ont été trouvées dans un descriptif.")
                    );
                }
            }

            // Verify language
            matcher = languagePattern.matcher(path);
            if (matcher.find()) {
                IsoCode639_1 lang = IsoCode639_1.valueOf(matcher.group(1).toUpperCase());
                if (!assertLanguage(lang, text)) {
                    anomalies.add(new Anomaly()
                        .setPath(path)
                        .setValue(text)
                        .setMessage("Mauvaise langue")
                        .setDescription(String.format("Ce texte ne semble pas être dans la langue spécifiée (%s).", lang))
                    );
                }
            }
        });

        return anomalies;
    }

    /**
     * @param text
     * @param expectedLang in ISO 639-1 (en, fr, es, etc)
     * @return
     */
    private Boolean assertLanguage(IsoCode639_1 expectedLang, String text) {
        Map<Language, Double> scores = detector.computeLanguageConfidenceValues(text);
        for (Map.Entry<Language, Double> score : scores.entrySet()) {
            if (score.getKey().getIsoCode639_1().equals(expectedLang) && score.getValue() >= minimumLanguageConfidence) {
                return true;
            }
        }

        return false;
    }
}
