/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import fr.datatourisme.quality.entity.Anomaly;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpTimeoutException;
import java.nio.channels.UnresolvedAddressException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 *  Test des Url cassées (404)
 */
@Component
@Log
public class LinksAnalyzer implements AnalyzerInterface {
    // connect timeout, in seconds
    private static final int TIMEOUT = 10;
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36";

    // cache status code
    Map<String, CompletableFuture<Integer>> cache = new HashMap<>();

    @Autowired
    AsyncTaskExecutor taskExecutor;

    @Override
    public List<Anomaly> analyze(Set<AnalyzeItem> items) {

        List<Task> tasks = new ArrayList<>();
        for (AnalyzeItem item : items) {
            Map<String, String> homepage =  item.getValues("//homepage");
            homepage.forEach((path, url) -> {
                CompletableFuture<Integer> future = getStatusCode(url);
                tasks.add(new Task(item, path, url, future));
            });
        }

        return tasks.stream()
            .map(this::handleTask)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    /**
     * Handle a task
     *
     * @param task
     * @return
     */
    private Anomaly handleTask(Task task) {
        Pair<String, String> message = null;
        try {
            // log.info("Waiting : " + task.getUrl());
            int statusCode = task.getFuture().get();
            if (statusCode >= 400) {
                message = Pair.of(String.format("Code erreur : %s", statusCode), String.format("Le serveur a renvoyé le code d'erreur HTTP %s", statusCode));
            }
        } catch (InterruptedException | ExecutionException exception) {
            try {
                if (exception.getCause() instanceof ConnectException && exception.getCause().getCause() != null) {
                    throw exception.getCause().getCause();
                } else {
                    throw exception.getCause();
                }
            } catch (UnresolvedAddressException e) {
                message = Pair.of("Hôte introuvable", "L'hôte n'a pas été trouvé");
            } catch (URISyntaxException e) {
                message = Pair.of("Adresse malformée", e.getMessage());
            } catch (HttpTimeoutException e) {
                message = Pair.of("Hôte injoignable", "L'hôte n'a pas répondu dans un délai de " +TIMEOUT + " secondes.");
            } catch (TimeoutException e) {
                message = Pair.of("Processus annulé", "Pour une raison inconnu, le processus de requête a été annulé");
            } catch (Throwable e) {
                message = Pair.of("Erreur de traitement", e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
            }
        }/* finally {
            log.info("Done : " + task.getUrl());
        }*/

        if (message != null) {
            return task.createAnomaly(message.getFirst(), message.getSecond());
        }

        return null;
    }

    /**
     * Return a status code for a given url
     * DO NOT USE DIRECTLY, use the futureCache::getUnchecked
     *
     * @param url
     * @return
     */
    private CompletableFuture<Integer> getStatusCode(String url) {
        // check the cache
        if (cache.containsKey(url)) {
            return cache.get(url);
        }

        final HttpClient httpClient =  HttpClient.newBuilder()
            .executor(taskExecutor)
            .followRedirects(HttpClient.Redirect.ALWAYS)
            .connectTimeout(Duration.of(TIMEOUT, ChronoUnit.SECONDS))
            .build();

        try {
            HttpRequest request = HttpRequest.newBuilder().GET()
                .header("user-agent", USER_AGENT)
                .timeout(Duration.of(TIMEOUT, ChronoUnit.SECONDS))
                .uri(new URI(url))
                .build();

            CompletableFuture<Integer> completableFuture = httpClient.sendAsync(request, HttpResponse.BodyHandlers.discarding())
                .orTimeout(5, TimeUnit.MINUTES)
                .thenApply(HttpResponse::statusCode);

            cache.put(url, completableFuture);

            return completableFuture;
        } catch (URISyntaxException e) {
            return CompletableFuture.failedFuture(e);
        }
    }

    @Data
    @AllArgsConstructor
    class Task {
        private AnalyzeItem item;
        private String path;
        private String url;
        private CompletableFuture<Integer> future;

        /**
         * Create an anomaly base on this task
         *
         * @param message
         * @return
         */
        Anomaly createAnomaly(String message, String description) {
            return new Anomaly()
                .setUri(item.getUri())
                .setPath(getPath())
                .setValue(url)
                .setMessage(message)
                .setDescription(description);
        }
    }
}
