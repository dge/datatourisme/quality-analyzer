/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import fr.datatourisme.quality.entity.Anomaly;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import org.apache.commons.jxpath.Pointer;
import org.geotools.data.collection.SpatialIndexFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ReprojectingFeatureCollection;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Contrôle de géolocalisation dans le polygone du département spécifié par le POI
 */
@Component
@Log
public class GeoCoordinatesAnalyzer extends AbstractAsyncAnalyzer {
    private final SpatialIndexFeatureCollection featureCollection;
    private final SpatialIndexFeatureCollection featureCollectionLambert;

    @SneakyThrows
    public GeoCoordinatesAnalyzer() {
        FeatureJSON gjson = new FeatureJSON();
        InputStream is = getClass().getClassLoader().getResourceAsStream("data/departements.geojson");
        SimpleFeatureCollection features = (SimpleFeatureCollection) gjson.readFeatureCollection(is);
        featureCollection = new SpatialIndexFeatureCollection(features);
        SimpleFeatureCollection sfc = new ReprojectingFeatureCollection(features, CRS.decode("EPSG:2154"));
        featureCollectionLambert = new SpatialIndexFeatureCollection(sfc);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Anomaly> analyze(AnalyzeItem item) {
        List<Anomaly> anomalies = new ArrayList<>();

        Map<String, Pointer> map = item.getPointers("isLocatedAt/geo");
        map.forEach((path, pointer) -> {
            String insee = item.getSingleValue(pointer, "../address/hasAddressCity/insee");
            Map<String, Object> geo = (Map) pointer.getValue();
            Number latitude = (Number) geo.get("latitude");
            Number longitude = (Number) geo.get("longitude");
            Anomaly anomaly = analyzeCoordinates(latitude.doubleValue(), longitude.doubleValue(), insee);
            if (anomaly != null) {
                anomalies.add(anomaly.setPath(path));
            }
        });

        return anomalies;
    }

    /**
     * @param latitude
     * @param longitude
     * @param insee
     * @throws Exception
     */
    @SneakyThrows
    private Anomaly analyzeCoordinates(Double latitude, Double longitude, String insee) {
        String expectedDpt = insee.substring(0, insee.startsWith("97") ? 3 : 2);

        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        Coordinate coords = new Coordinate(longitude, latitude);
        Point point = geometryFactory.createPoint(coords);

        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
        Filter filter = ff.contains(ff.property( "geometry"), ff.literal( point ));
        SimpleFeatureCollection filtered = featureCollection.subCollection(filter);

        try (SimpleFeatureIterator iterator = filtered.features()) {
            if (!iterator.hasNext()) {
                return analyzeCoordinatesWithBuffer(latitude, longitude, insee, 2000d);
            }

            SimpleFeature feature = iterator.next();
            String actualDpt = (String) feature.getAttribute("INSEE_DEP");
            if (!expectedDpt.equals(actualDpt)) {
                return new Anomaly()
                    .setMessage("Hors du département")
                    .setDescription(String.format("Les coordonnées se situent dans le département %s, elles devraient se situer dans le département %s", actualDpt, expectedDpt));
            }
        }

        return null;
    }

    /**
     * @param latitude
     * @param longitude
     * @param insee
     * @param distance in meters
     * @throws Exception
     */
    @SneakyThrows
    private Anomaly analyzeCoordinatesWithBuffer(Double latitude, Double longitude, String insee, double distance) {
        String expectedDpt = insee.substring(0, 2);

        CoordinateReferenceSystem sourceCRS = CRS.decode("epsg:4326");
        CoordinateReferenceSystem targetCRS = CRS.decode("epsg:2154");
        MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS, false);

        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        // https://blog.paloo.fr/index.php/posts/longitude-x-latitude-y-ou-pas
        // https://stackoverflow.com/questions/63091589/geotools-reversing-lat-lon-for-wgs-when-transforming-between-projections
        Coordinate coords = JTS.transform(new Coordinate(latitude, longitude), null, transform);
        Point point = geometryFactory.createPoint(coords);

        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
        Filter filter = ff.dwithin(ff.property("geometry"), ff.literal(point), distance, "m");
        SimpleFeatureCollection filtered = featureCollectionLambert.subCollection(filter);

        try (SimpleFeatureIterator iterator = filtered.features()) {
            if (!iterator.hasNext()) {
                return new Anomaly()
                    .setMessage("Hors du territoire national")
                    .setDescription("Les coordonnées ne se situent dans aucun département");
            }

            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                String actualDpt = (String) feature.getAttribute("INSEE_DEP");
                if (expectedDpt.equals(actualDpt)) {
                    return null;
                }
            }
        }

        return new Anomaly()
            .setMessage("Hors du département")
            .setDescription(String.format("Les coordonnées ne se situent pas à proximité du département %s", expectedDpt));
    }
}
