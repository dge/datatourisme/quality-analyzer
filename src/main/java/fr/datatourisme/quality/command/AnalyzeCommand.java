/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.command;

import fr.datatourisme.quality.analyzer.AnalyzeItem;
import fr.datatourisme.quality.analyzer.AnalyzerInterface;
import fr.datatourisme.quality.analyzer.LinksAnalyzer;
import fr.datatourisme.quality.entity.Anomaly;
import fr.datatourisme.quality.entity.PointOfInterest;
import fr.datatourisme.quality.repository.PointOfInterestRepository;
import fr.datatourisme.quality.vocabulary.Datatourisme;
import lombok.SneakyThrows;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.convert.ElasticsearchConverter;
import org.springframework.data.elasticsearch.core.index.MappingBuilder;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model;
import picocli.CommandLine.Spec;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Command(
    name = "analyze",
    description = "Launch a daily analyze of points of interest",
    mixinStandardHelpOptions = true
)
public class AnalyzeCommand implements Runnable {

    @Spec
    Model.CommandSpec spec;

    @Autowired
    RestHighLevelClient client;

    @Autowired
    ElasticsearchOperations elasticsearchOperations;

    @Autowired
    List<AnalyzerInterface> analyzers;

    @Autowired
    private PointOfInterestRepository pointOfInterestRepository;

    @Autowired
    AsyncTaskExecutor taskExecutor;

    @Autowired
    ElasticsearchConverter elasticsearchConverter;

    @Autowired
    OntModel ontModel;

    List<List<OntClass>> types;

    @SneakyThrows
    @Override
    public void run() {
        IndexCoordinates indexCoordinates = elasticsearchOperations.getIndexCoordinatesFor(PointOfInterest.class);

        // update mapping
        updatePointOfInterestMapping();

        // scroll elements to analyze
        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(10L));
        SearchRequest searchRequest = new SearchRequest(indexCoordinates.getIndexNames());
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.size(200);
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(scroll);
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        String scrollId = searchResponse.getScrollId();
        SearchHit[] searchHits = searchResponse.getHits().getHits();

        ProgressBarBuilder pbb = new ProgressBarBuilder()
            .setStyle(ProgressBarStyle.UNICODE_BLOCK)
            .setTaskName("Analyze in progress...")
            .setUnit(" objects", 1)
            .setInitialMax(searchResponse.getHits().getTotalHits().value)
            .showSpeed();

        ProgressBar pb = pbb.build();
        pb.stepTo(0);

        while (searchHits != null && searchHits.length > 0) {
            List<CompletableFuture<List<Anomaly>>> futures = new ArrayList<>();

            // prepare items
            Set<AnalyzeItem> items = Arrays.stream(searchHits).map(searchHit -> {
                return AnalyzeItem.create(searchHit.getId(), searchHit.getSourceAsMap());
            }).collect(Collectors.toSet());

            // call analyzers
            for (AnalyzerInterface analyzer : analyzers) {
                if (!(analyzer instanceof LinksAnalyzer)) {
                    CompletableFuture<List<Anomaly>> future = CompletableFuture.supplyAsync(() -> analyzer.analyze(items), taskExecutor)
                        .thenApply(anomalies -> {
                            anomalies.forEach(anomaly -> anomaly.setAnalyzer(analyzer.getClass().getSimpleName()));
                            return anomalies;
                        });

                    futures.add(future);
                }
            }

            // process anomaly records
            List<Anomaly> anomalies = futures.stream()
                .map(CompletableFuture::join)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

            // update indices
            updatePointOfInterestIndex(items, anomalies);

            pb.stepBy(searchHits.length);
            System.gc();

            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
            scrollId = searchResponse.getScrollId();
            searchHits = searchResponse.getHits().getHits();
        }

        pb.close();
    }

    /**
     * Update index mapping according to spring annotations
     */
    @SneakyThrows
    private void updatePointOfInterestMapping() {
        IndexCoordinates indexCoordinates = elasticsearchOperations.getIndexCoordinatesFor(PointOfInterest.class);
        MappingBuilder mappingBuilder = new MappingBuilder(elasticsearchConverter);
        String mapping = mappingBuilder.buildPropertyMapping(PointOfInterest.class);

        // remove useless _class property
        mapping = mapping.replace("\"_class\":{\"type\":\"keyword\",\"index\":false,\"doc_values\":false},", "");

        PutMappingRequest request = new PutMappingRequest(indexCoordinates.getIndexNames());
        request.source(mapping, XContentType.JSON);

        client.indices().putMapping(request, RequestOptions.DEFAULT);
    }

    /**
     * Update POI index
     *
     * @param anomalies
     */
    private void updatePointOfInterestIndex(Set<AnalyzeItem> items, List<Anomaly> anomalies) {
        Map<String, List<Anomaly>> anomaliesMap = anomalies.stream()
            .collect(Collectors.groupingBy(Anomaly::getUri));

        List<PointOfInterest> points = items.stream().map(item -> {
            List<Anomaly> objectAnomalies = anomaliesMap.getOrDefault(item.getUri(), List.of());
            return processItem(item, objectAnomalies);
        }).collect(Collectors.toList());

        pointOfInterestRepository.update(points);
    }

    /**
     * Process a single item
     *
     * @param item
     * @param anomalies
     * @return
     */
    private PointOfInterest processItem(AnalyzeItem item, List<Anomaly> anomalies) {
        PointOfInterest poi = new PointOfInterest();
        poi.setUri(item.getUri());
        PointOfInterest.AnalyzeRecord analyzeObj = poi.getAnalyze();

        // types
        Map<String, String> typesMap =  item.getValues("/type");
        analyzeObj.setType1(getTypeLabels(typesMap.values(), 0 ));
        analyzeObj.setType2(getTypeLabels(typesMap.values(), 1 ));
        analyzeObj.setType3(getTypeLabels(typesMap.values(), 2 ));

        // count anomalies
        Map<String, Integer> anomalyStatistics = new HashMap<>();
        anomalies.stream()
            .collect(Collectors.groupingBy(Anomaly::getAnalyzer))
            .forEach((s, list) -> anomalyStatistics.put(s, list.size()));
        anomalyStatistics.put("total", anomalies.size());
        analyzeObj.setAnomalyStatistics(anomalyStatistics);

        // count medias
        analyzeObj.setMediaCount(item.getValues("hasRepresentation").size());

        // set anomalies
        analyzeObj.setAnomalies(anomalies);

        return poi;
    }

    /**
     * Get types level index
     *
     * @param index
     * @return
     */
    private List<String> getTypeLabels(Collection<String> list, int index) {
        if (types == null) {
            types = new ArrayList<>();
            List<OntClass> level2 = new ArrayList<>();
            List<OntClass> level3 = new ArrayList<>();
            OntClass ontClass = ontModel.getOntClass(Datatourisme.PointOfInterest.getURI());
            types.add(ontClass.listSubClasses(true).toList());
            ontClass.listSubClasses(true).forEach(ontClass2 -> {
                level2.addAll(ontClass2.listSubClasses(true).toList());
                ontClass2.listSubClasses(true).forEach(ontClass3 -> {
                    level3.addAll(ontClass3.listSubClasses(true).toList());
                });
            });
            types.add(level2);
            types.add(level3);
        }

        return types.get(index).stream()
            .filter(ontClass -> list.contains(ontClass.getURI()))
            .map(ontClass -> ontClass.getLabel("fr"))
            .collect(Collectors.toList());
    }
}
