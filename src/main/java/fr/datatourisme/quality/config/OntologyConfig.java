/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.config;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.InputStream;

@Configuration
public class OntologyConfig {

    static {
        // JenaParameters.
    }

    @Bean
    public OntModel ontology() {
        InputStream is = getClass().getClassLoader().getResourceAsStream("datatourisme.ttl");
        Model model = ModelFactory.createDefaultModel();
        RDFDataMgr.read(model, is, Lang.TTL);

        OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_LITE_MEM);
        ontModel.getDocumentManager().setProcessImports(false);
        ontModel.add(model);

        return ontModel;
    }
}
