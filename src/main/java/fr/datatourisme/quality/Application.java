/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality;

import fr.datatourisme.quality.command.AnalyzeCommand;
import fr.datatourisme.quality.command.PurgeCommand;
import fr.datatourisme.quality.command.StatisticsCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import picocli.CommandLine;
import picocli.CommandLine.Model;
import picocli.CommandLine.Spec;
import picocli.spring.PicocliSpringFactory;

import java.time.LocalDate;

@SpringBootApplication
@CommandLine.Command(
	name = "quality-cli",
	mixinStandardHelpOptions = true,
    subcommands = {
        AnalyzeCommand.class,
        PurgeCommand.class,
		StatisticsCommand.class
    }
)
public class Application implements CommandLineRunner, ExitCodeGenerator, Runnable {
	private int exitCode;

	@Spec
	Model.CommandSpec spec;

	@Autowired
	private PicocliSpringFactory factory;

	/**
	 * Main static method
	 */
	public static void main(String[] args) {
		System.exit(SpringApplication.exit(SpringApplication.run(Application.class, args)));
	}

	/**
	 * From Runnable interface
	 */
	@Override
	public void run() {
		throw new CommandLine.ParameterException(spec.commandLine(), "Missing required subcommand");
	}

	/**
	 * From CommandLineRunner interface
	 */
	@Override
	public void run(String... args) throws Exception {
		exitCode = new CommandLine(new Application(), factory).execute(args);
	}

	/**
	 * From ExitCodeGenerator interface
	 */
	@Override
	public int getExitCode() {
		return exitCode;
	}

	/**
	 * A simple bean to provide a consistent start date
	 */
	@Bean
	public LocalDate startDate() {
		return LocalDate.now();
	}
}
