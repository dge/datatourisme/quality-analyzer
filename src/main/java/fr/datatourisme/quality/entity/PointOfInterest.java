/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Document(indexName = "datatourisme")
@Data
public class PointOfInterest {
    @Id
    @Transient
    private String uri;

    @Field(type = FieldType.Object, name = "analyze")
    private AnalyzeRecord analyze = new AnalyzeRecord();

    @Data
    @Accessors(chain = true)
    static public class AnalyzeRecord {
        @Field(type = FieldType.Keyword)
        private List<String> type1;

        @Field(type = FieldType.Keyword)
        private List<String> type2;

        @Field(type = FieldType.Keyword)
        private List<String> type3;

        @Field(type = FieldType.Object)
        private Map<String, Integer> anomalyStatistics;

        @Field(type = FieldType.Integer)
        private int mediaCount;

        @Field(type = FieldType.Nested, includeInParent = true)
        private Collection<Anomaly> anomalies;
    }
}
