/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import org.geotools.data.collection.SpatialIndexFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ReprojectingFeatureCollection;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import java.io.IOException;
import java.io.InputStream;

class GeoCoordinatesAnalyzerTest {

    @Test
    void analyze() throws IOException, FactoryException, TransformException {
        FeatureJSON gjson = new FeatureJSON();
        InputStream is = getClass().getClassLoader().getResourceAsStream("data/departements.geojson");
        SimpleFeatureCollection features = (SimpleFeatureCollection) gjson.readFeatureCollection(is);
        SpatialIndexFeatureCollection featureCollection = new SpatialIndexFeatureCollection(features);

        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        // Coordinate coord = new Coordinate(-1.6777926, 48.117266);
        // Coordinate coord = new Coordinate(	55.450675, -20.882057);
        Coordinate coord = new Coordinate(	  -1.5114, 48.636);
        Point point = geometryFactory.createPoint(coord);

        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
        Filter filter = ff.contains(ff.property( "geometry"), ff.literal( point ));
        System.out.println(filter);
        SimpleFeatureCollection test = featureCollection.subCollection(filter);

        SimpleFeatureIterator iter = test.features();
        while (iter.hasNext()) {
            SimpleFeature feature = iter.next();
            System.out.println(feature.getAttribute("INSEE_DEP"));
        }
    }

    @Test
    void analyzeWithBuffer() throws IOException, FactoryException, TransformException {
        FeatureJSON gjson = new FeatureJSON();
        InputStream is = getClass().getClassLoader().getResourceAsStream("data/departements.geojson");
        SimpleFeatureCollection features = (SimpleFeatureCollection) gjson.readFeatureCollection(is);

        SimpleFeatureCollection sfc = new ReprojectingFeatureCollection(features, CRS.decode("EPSG:2154"));
        SpatialIndexFeatureCollection featureCollection = new SpatialIndexFeatureCollection(sfc);

        CoordinateReferenceSystem sourceCRS = CRS.decode("epsg:4326");
        CoordinateReferenceSystem targetCRS = CRS.decode("epsg:2154");
        MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS, false);


        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        // Coordinate coord = new Coordinate(-1.6777926, 48.117266);
        // Coordinate coord = new Coordinate(	55.450675, -20.882057);
        // https://blog.paloo.fr/index.php/posts/longitude-x-latitude-y-ou-pas
        // https://stackoverflow.com/questions/63091589/geotools-reversing-lat-lon-for-wgs-when-transforming-between-projections
        Coordinate coord = JTS.transform(new Coordinate(48.636, -1.5114), null, transform);
        Point point = geometryFactory.createPoint(coord);




        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
        double distance = 1000d;
        Filter filter = ff.dwithin(ff.property("geometry"), ff.literal(point), distance, "m");
        System.out.println(filter);
        SimpleFeatureCollection test = featureCollection.subCollection(filter);

        SimpleFeatureIterator iter = test.features();
        while (iter.hasNext()) {
            SimpleFeature feature = iter.next();
            System.out.println(feature.getAttribute("INSEE_DEP"));
        }
    }
}