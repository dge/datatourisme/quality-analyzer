/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import com.jayway.jsonpath.JsonPath;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *  https://pythonrepo.com/repo/pemistahl-lingua-python-computer-vision
 *  https://github.com/pemistahl/lingua
 */
class CustomTest {
    @Test
    void jsonPath() {
        Map<String, Object> map = Map.of("root", List.of(
            Map.of("test", List.of(Map.of("value", 1), Map.of("value", 2))),
            Map.of("test", Map.of("value", 3))
        ));



        Object test = JsonPath.read(map, "root.*.test..value");
        System.out.println(test);
    }

    @Test
    void jxPath() {
        Map<String, Object> map = Map.of("root", List.of(
            Map.of("test", List.of(Map.of("value", 1), Map.of("value", 2))),
            Map.of("test2", List.of("haha", "hoho"))
        ));

        JXPathContext ctx = JXPathContext.newContext(map);
        Iterator<Pointer> test = ctx.iteratePointers("root/test2");

        test.forEachRemaining(o -> {
            System.out.println(o.getValue());
        });
        // Pointer test = org.apache.commons.jxpath.JXPathContext.newContext(map).getPointer("root/test/value");

    }
}